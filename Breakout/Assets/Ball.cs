﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	Rigidbody2D rb2d;
	CircleCollider2D coll2d;
	public Vector2 initialDirection;
	public GameController gameController;
	public float initialSpeed;
	public float speedIncrement;
	float currentSpeed;
	public float minGradient; // minimum allowed velocity gradient (to avoid getting stuck)
	public float maxGradient; // max allowed velocity gradient

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
		rb2d.velocity = initialDirection * initialSpeed;
		currentSpeed = initialSpeed;
	}
		
	public void SetWidth(float width) {
		transform.localScale = width * Vector3.one;
	}

	void Update() {
		Vector2 viewportPoint = Camera.main.WorldToViewportPoint(transform.position);
		viewportPoint -= new Vector2(0.5f, 0.5f);
		float screenX = viewportPoint.x;
		float screenY = viewportPoint.y;

		if (Mathf.Abs(screenX) > 0.5f) {
			Vector2 normal = (Vector2.left * screenX).normalized;
			Bounce(rb2d.velocity, normal);
		} else if (screenY > 0.5f) {
			Vector2 normal = (Vector2.down * screenY).normalized;
			Bounce(rb2d.velocity, normal);
		} else if (screenY < -0.5f) {
			gameController.BallLost();
		}
	}

	void OnCollisionEnter2D(Collision2D coll) {
		ContactPoint2D[] contactPoints = new ContactPoint2D[10];
		coll.GetContacts(contactPoints);

		Vector2 collDirection = -coll.relativeVelocity;
		Vector2 normal = contactPoints[0].normal;
		Bounce(collDirection, normal);

		if (coll.collider.tag == "Brick") {
			coll.collider.GetComponent<Brick>().HitBrick();
			gameController.ScoreUp(50);
			currentSpeed += speedIncrement;
		}
	}

	void Bounce(Vector2 inDirection, Vector2 normal) {
		if (Vector2.Dot(inDirection, normal) < 0f) {
			Vector2 newDirection = Vector2.Reflect(inDirection, normal);

			float xVelocity = newDirection.x;
			float yVelocity = newDirection.y;
			if (xVelocity == 0f) {
				xVelocity = 0.01f;
			}
			float gradient = Mathf.Abs(yVelocity / xVelocity);

			if (gradient < minGradient) {
				yVelocity = Mathf.Sign(yVelocity) * (minGradient * xVelocity);
			} else if (gradient > maxGradient) {
				xVelocity = Mathf.Sign(xVelocity) * (yVelocity / maxGradient);
			}

			Vector2 newVelocity = new Vector2(xVelocity, yVelocity);
			rb2d.velocity = newVelocity.normalized * currentSpeed;
		}
	}
}
