﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public GameObject brickPrefab;
	public Text scoreText;
	public Paddle paddle;
	public Ball ball;
	public int rows = 6;
	public int columns = 12;
	public float firstRowHeight;
	public float brickHeight;

	public float paddleWidth;
	public float ballWidth;

	Color32[] brickColors = {
		new Color32(195, 146, 225, 255),
		new Color32(142, 194, 254, 255),
		new Color32(142, 255, 163, 255),
		new Color32(255, 255, 147, 255),
		new Color32(255, 192, 148, 255),
		new Color32(255, 83, 85, 255),
		new Color32(210, 211, 213, 255),
		new Color32(244, 246, 244, 255)
	};

	int score = 0;

	// Use this for initialization
	void Start () {
		paddle.SetWidth(paddleWidth);
		ball.SetWidth(ballWidth);

		Vector2 screenSizeUnits = 2 * Camera.main.ViewportToWorldPoint(Vector2.one);

		float brickWidth = screenSizeUnits.x / columns;

		brickPrefab.transform.localScale = new Vector3(brickWidth, brickHeight);


		float firstColumnX = -screenSizeUnits.x / 2f + brickWidth / 2f;
		Vector2 instancePosition = new Vector2(firstColumnX, firstRowHeight);

		int i = 0;
		while (i < rows) {
			int j = 0;
			while (j < columns) {
				GameObject brickClone = (GameObject)Instantiate(brickPrefab, instancePosition, Quaternion.identity);
				brickClone.GetComponent<SpriteRenderer>().color = brickColors[i];
				instancePosition = new Vector2(instancePosition.x + brickWidth, instancePosition.y);
				j++;
			}
			instancePosition = new Vector2(firstColumnX, instancePosition.y + brickHeight);
			i++;
		}
	}

	public void ScoreUp(int points) {
		score += points;
		scoreText.text = "Score: " + score;
		PlayerPrefs.SetInt("hiscore", score);
	}

	public void BallLost() {
		SceneManager.LoadScene(0);
	}
}
