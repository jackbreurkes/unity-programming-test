﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

	public Text hiscore;

	// Use this for initialization
	void Start () {
		hiscore.text = "highscore: " + PlayerPrefs.GetInt("hiscore", 0);
	}

	public void StartGame() {
		SceneManager.LoadScene(1);
	}
}
